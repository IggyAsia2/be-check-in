const { DataTypes } = require("sequelize");
const catchAsync = require("../../utils/catchAsync");

const ModelCreate = (sequelize) => {
  const Date = sequelize.define(
    "Date",
    {
      date: {
        type: DataTypes.DATEONLY,
        allowNull: false,
        unique: true,
      },
      name: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      note: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      trial962: {
        type: DataTypes.STRING(1),
      },
    },
    {
      tableName: "t_specialday",
      timestamps: false,
    }
  );

  const Student = sequelize.define(
    "Student",
    {
      // id: {
      //   type: DataTypes.INTEGER,
      //   autoIncrement: true,
      //   primaryKey: true,
      // },
      code: {
        type: DataTypes.STRING(50),
        // unique: true,
      },
      name: {
        type: DataTypes.STRING(50),
      },
      gender: {
        type: DataTypes.BOOLEAN,
      }
    },
    { tableName: "t_student", timestamps: false }
  );

  const StudentAttendance = sequelize.define(
    "StudentAttendance",
    {
      status: {
        type: DataTypes.STRING(100),
        allowNull: false,
      },
      note: {
        type: DataTypes.STRING,
      },
      createdby: {
        type: DataTypes.STRING,
      },
      date: {
        type: DataTypes.DATEONLY,
        allowNull: false,
      },
      trial994: {
        type: DataTypes.STRING(1),
      },
      // studentid: {
      //   type: DataTypes.INTEGER,
      // },
    },
    {
      tableName: "t_student_attendance",
      timestamps: true,
      createdAt: "createdat",
      updatedAt: "updatedat",
    }
  );

  Student.hasMany(StudentAttendance);

  StudentAttendance.belongsTo(Student, { foreignKey: "studentid" });

  // sequelize.sync({ alter: true });
};

module.exports = ModelCreate;
