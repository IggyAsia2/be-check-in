FROM node:14-alpine

WORKDIR /app

COPY . .

RUN chown -R node:node /app
USER node

RUN yarn

CMD ["yarn","start"]