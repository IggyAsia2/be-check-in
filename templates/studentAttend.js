const excelJS = require("exceljs");

const boldFont = {
  name: "Arial",
  size: 10,
  bold: true,
};
const italicFont = {
  name: "Arial",
  size: 10,
  italic: true,
};
const defautFont = {
  name: "Arial",
  size: 10,
};
const cenMid = {
  vertical: "middle",
  horizontal: "center",
};
const topMid = {
  vertical: "top",
  horizontal: "center",
};

const x = {
  label: "",
  val: "X",
  color: "FFFFFF",
};

const t = {
  label: "- Đi trễ",
  val: "T",
  color: "FB1111",
};

const xt = {
  label: "- Xin đi trễ",
  val: "XT",
  color: "EDE733",
};

const nk = {
  label: "- Nghỉ không lương",
  val: "NK",
  color: "FFFFFF",
};

const k = {
  label: "- Nghỉ không phép",
  val: "K",
  color: "6D0202",
};

const p = {
  label: "- Nghỉ phép",
  val: "P",
  color: "4DDD25",
};

const p12 = {
  label: "- Nghỉ nửa ngày tính phép",
  val: "1/2P",
  color: "F3E7DA",
};

const nn = {
  label: "- Làm nửa ngày công",
  val: "NN",
  color: "D93BDC",
};

const n = {
  label: "- Ngừng việc",
  val: "N",
  color: "3B78DC",
};

// const optionsOffDayArr = [x, t, xt, nk, k, p, p12, nn, n];

// const optionsOffDayObj = {
//   X: x,
//   T: t,
//   XT: xt,
//   NK: nk,
//   K: k,
//   P: p,
//   "1/2P": p12,
//   NN: nn,
//   N: n,
// };

const handleOffDayArr = (data) => {
  return data.map((el) => {
    return {
      label: el.name,
      val: el.val,
      color: el.config.bg,
    };
  });
};

const handleOffDayObj = (data) => {
  return data.reduce(
    (a, v) => ({
      ...a,
      [v.val]: {
        label: v.name,
        val: v.val,
        color: v.config.bg,
      },
    }),
    {}
  );
};

const studentAttend = ({
  from_day,
  days,
  arrDays,
  bigData,
  workbook,
  options,
  numNL,
}) => {
  const optionsOffDayArr = handleOffDayArr(options);
  const optionsOffDayObj = handleOffDayObj(options);
  const sheet = workbook.addWorksheet(`Tháng ${from_day.getMonth()}`);

  // header

  const dayColumns = arrDays.map((el) => {
    return { width: 4.33 };
  });

  sheet.columns = [
    { width: 8.67 },
    { width: 24.13 },
    { width: 8.4 },
    ...dayColumns,
  ];

  // Row 1
  sheet.getRow(1).font = {
    name: "Arial",
    size: 9,
    bold: true,
  };
  sheet.mergeCells("B1:AN1");
  sheet.getCell("B1").value = "TÊN ĐƠN VỊ: ĐIỆN TOÁN GROUP";

  // Row 2
  sheet.getRow(2).font = {
    name: "Arial",
    size: 16,
    bold: true,
  };
  sheet.getRow(2).alignment = {
    vertical: "bottom",
    horizontal: "center",
  };
  sheet.mergeCells("A2:AN2");
  sheet.getCell("A2").value = "BẢNG CHẤM CÔNG";

  // Row 3
  sheet.getRow(3).font = {
    name: "Arial",
    size: 10,
  };
  sheet.getRow(3).alignment = {
    vertical: "bottom",
    horizontal: "center",
  };
  sheet.mergeCells("A3:AN3");
  sheet.getCell(
    "A3"
  ).value = `Tháng ${from_day.getMonth()} năm ${from_day.getFullYear()}`;

  // Row 5 6
  sheet.getRow(5).font = boldFont;
  sheet.getRow(6).font = boldFont;
  sheet.getRow(5).alignment = cenMid;
  sheet.getRow(6).alignment = cenMid;
  sheet.mergeCells("A5:A6");
  sheet.mergeCells("B5:B6");
  sheet.mergeCells("C5:C6");
  sheet.getCell("A5").value = "TT";
  sheet.getCell("B5").value = "Họ và tên";
  sheet.getCell("C5").value = "Thuộc";

  if (days > 22) {
    sheet.mergeCells(
      `D5:A${String.fromCharCode("A".charCodeAt(0) + days - 23)}5`
    );
  } else {
    sheet.mergeCells(`D5:${String.fromCharCode("D".charCodeAt(0) + days)}5`);
  }
  sheet.getCell("D5").value = "Ngày trong tháng";

  // Fomula
  sheet.getColumn("AJ").width = 16.67;
  sheet.mergeCells("AJ5:AJ6");
  sheet.getCell("AJ5").value = "Tổng ngày đi trễ";

  sheet.mergeCells("AK5:AM5");
  sheet.getCell("AK5").value = "Ngày nghỉ";
  sheet.getCell("AK6").value = "Nghỉ lễ";
  sheet.getCell("AL6").value = "Có phép";
  sheet.getCell("AM6").value = "Không phép";

  sheet.getColumn("AN").width = 16.67;
  sheet.mergeCells("AN5:AN6");
  sheet.getCell("AN5").value = "Tổng số ngày làm việc";

  sheet.getColumn("AO").width = 16.67;
  sheet.mergeCells("AO5:AO6");
  sheet.getCell("AO5").value = "Tổng ngày WFH";

  sheet.getColumn("AP").width = 16.67;
  sheet.mergeCells("AP5:AP6");
  sheet.getCell("AP5").value = "Tổng số ngàylàm việc tại cty";

  const startCol = "D";
  arrDays.forEach((el, index) => {
    if (index > 22) {
      sheet.getCell(
        `A${String.fromCharCode("A".charCodeAt(0) + index - 23)}6`
      ).value = new Date(el).getDate();
    } else {
      sheet.getCell(
        `${String.fromCharCode(startCol.charCodeAt(0) + index)}6`
      ).value = new Date(el).getDate();
    }
  });

  // middle
  let startRow = 7;
  let subNumUser = 0;
  bigData.forEach((data, bigIndex) => {
    const numUser = data.length / (days + 1);
    let count = 0;
    let subCount = startRow;
    sheet.mergeCells(`C${startRow}:C${startRow + numUser - 1}`);
    sheet.getCell(`C${startRow}`).alignment = cenMid;
    sheet.getCell(`C${startRow}`).value = data[0].orgunit;

    for (let i = 0; i <= days; i++) {
      const offDay = data[i].attendance;
      if (offDay === "T7" || offDay === "CN" || offDay === "NL") {
        const alp = (char) => String.fromCharCode(char.charCodeAt(0) + i);
        if (i > 22) {
          sheet.mergeCells(
            `A${String.fromCharCode(
              "A".charCodeAt(0) + i - 23
            )}${startRow}:A${String.fromCharCode("A".charCodeAt(0) + i - 23)}${
              startRow + numUser - 1
            }`
          );
        } else {
          sheet.mergeCells(
            `${alp(startCol)}${startRow}:${alp(startCol)}${
              startRow + numUser - 1
            }`
          );
        }
      }
    }

    for (let i = 0; i < numUser; i++) {
      sheet.getRow(startRow + i).font = defautFont;
      sheet.getCell(`A${startRow + i}`).alignment = cenMid;
      sheet.getCell(`A${startRow + i}`).value = 1 + subNumUser;

      subNumUser++;

      sheet.getCell(`B${startRow + i}`).value = data[count].studentname;

      let countT = 0;
      let countP = 0;
      let countK = 0;
      let countWFH = 0;

      for (let j = 0; j <= days; j++) {
        const offDay = data[count + j].attendance;

        // Calculate
        switch (offDay) {
          case "T":
            countT++;
            break;
          case "P":
            countP++;
            break;
          case "K":
            countK++;
            break;
          case "WFH":
            countWFH++;
            break;
        }

        // Render cell
        let eachDay;
        let holDay;

        if (j > 22) {
          eachDay = sheet.getCell(
            `A${String.fromCharCode(
              `${"A".charCodeAt(0) + j - 23}`
            )}${subCount}`
          );
          if (bigIndex == 0) {
            holDay = sheet.getCell(
              `A${String.fromCharCode(
                `${"A".charCodeAt(0) + j - 23}`
              )}${startRow}`
            );
          }
        } else {
          eachDay = sheet.getCell(
            `${String.fromCharCode(startCol.charCodeAt(0) + j)}${subCount}`
          );
          if (bigIndex == 0) {
            holDay = sheet.getCell(
              `${String.fromCharCode(startCol.charCodeAt(0) + j)}${startRow}`
            );
          }
        }
        if (bigIndex == 0) {
          switch (offDay) {
            case "T7":
              holDay.value = offDay;
              break;
            case "CN":
              holDay.value = offDay;
              break;
            case "NL":
              holDay.value = offDay;
              break;
          }
        }

        eachDay.alignment = topMid;

        if (
          offDay !== "T7" &&
          offDay !== "CN" &&
          offDay !== "NL" &&
          offDay !== ""
        ) {
          if (offDay !== "X" && offDay !== "NK") {
            eachDay.fill = {
              type: "pattern",
              pattern: "solid",
              fgColor: { argb: `${optionsOffDayObj[offDay].color}` },
            };
          }
          eachDay.value = offDay;
        }
      }

      const realWorkDay = days + 1 - numNL - countP - countK;

      sheet.getCell(`AJ${startRow + i}`).value = countT;
      sheet.getCell(`AK${startRow + i}`).value = numNL;
      sheet.getCell(`AL${startRow + i}`).value = countP;
      sheet.getCell(`AM${startRow + i}`).value = countK;
      sheet.getCell(`AN${startRow + i}`).value = realWorkDay;
      sheet.getCell(`AO${startRow + i}`).value = countWFH;
      sheet.getCell(`AP${startRow + i}`).value = realWorkDay - countWFH;

      subCount = subCount + 1;
      count = count + days + 1;
    }
    startRow += numUser;
  });

  // footer

  const iRow = 12;

  sheet.mergeCells(`B${subNumUser + iRow}:L${subNumUser + iRow}`);
  sheet.getCell(`B${subNumUser + iRow}`).font = boldFont;
  sheet.getCell(`B${subNumUser + iRow}`).value = `Ký hiệu chấm công`;

  sheet.mergeCells(`A${subNumUser + iRow + 1}:B${subNumUser + iRow + 1}`);
  sheet.getCell(`A${subNumUser + iRow + 1}`).font = defautFont;
  sheet.getCell(`A${subNumUser + iRow + 1}`).value = `- Thứ 7`;

  sheet.mergeCells(`A${subNumUser + iRow + 2}:B${subNumUser + iRow + 2}`);
  sheet.getCell(`A${subNumUser + iRow + 2}`).font = defautFont;
  sheet.getCell(`A${subNumUser + iRow + 2}`).value = `- Chủ nhật`;

  sheet.mergeCells(`A${subNumUser + iRow + 3}:B${subNumUser + iRow + 3}`);
  sheet.getCell(`A${subNumUser + iRow + 3}`).font = defautFont;
  sheet.getCell(`A${subNumUser + iRow + 3}`).value = `- Nghỉ lễ`;

  sheet.getCell(`C${subNumUser + iRow + 1}`).font = defautFont;
  sheet.getCell(`C${subNumUser + iRow + 1}`).value = `T7`;
  sheet.getCell(`C${subNumUser + iRow + 2}`).font = defautFont;
  sheet.getCell(`C${subNumUser + iRow + 2}`).value = `CN`;
  sheet.getCell(`C${subNumUser + iRow + 3}`).font = defautFont;
  sheet.getCell(`C${subNumUser + iRow + 3}`).value = `NL`;

  optionsOffDayArr.forEach((el, index) => {
    sheet.mergeCells(
      `D${subNumUser + iRow + 1 + index}:J${subNumUser + iRow + 1 + index}`
    );
    sheet.getCell(`D${subNumUser + iRow + 1 + index}`).font = defautFont;
    sheet.getCell(`D${subNumUser + iRow + 1 + index}`).value = el.label;

    sheet.mergeCells(
      `K${subNumUser + iRow + 1 + index}:L${subNumUser + iRow + 1 + index}`
    );
    sheet.getCell(`K${subNumUser + iRow + 1 + index}`).font = defautFont;
    sheet.getCell(`K${subNumUser + iRow + 1 + index}`).fill = {
      type: "pattern",
      pattern: "solid",
      fgColor: { argb: el.color },
    };
    sheet.getCell(`K${subNumUser + iRow + 1 + index}`).alignment = cenMid;
    sheet.getCell(`K${subNumUser + iRow + 1 + index}`).value = el.val;
  });

  sheet.mergeCells(`O${subNumUser + iRow + 1}:T${subNumUser + iRow + 1}`);
  sheet.getCell(`O${subNumUser + iRow + 1}`).font = boldFont;
  sheet.getCell(`O${subNumUser + iRow + 1}`).alignment = cenMid;
  sheet.getCell(`O${subNumUser + iRow + 1}`).value = `Người chấm công`;

  sheet.mergeCells(`O${subNumUser + iRow + 2}:T${subNumUser + iRow + 2}`);
  sheet.getCell(`O${subNumUser + iRow + 2}`).font = italicFont;
  sheet.getCell(`O${subNumUser + iRow + 2}`).alignment = cenMid;
  sheet.getCell(`O${subNumUser + iRow + 2}`).value = `(Ký, họ tên)`;

  sheet.mergeCells(`O${subNumUser + iRow + 7}:T${subNumUser + iRow + 7}`);
  sheet.getCell(`O${subNumUser + iRow + 7}`).font = defautFont;
  sheet.getCell(`O${subNumUser + iRow + 7}`).alignment = cenMid;
  sheet.getCell(`O${subNumUser + iRow + 7}`).value = `Trần Nguyễn Khánh Linh`;

  sheet.mergeCells(`V${subNumUser + iRow - 1}:AH${subNumUser + iRow - 1}`);
  sheet.getCell(`V${subNumUser + iRow - 1}`).font = defautFont;
  sheet.getCell(`V${subNumUser + iRow - 1}`).alignment = cenMid;
  sheet.getCell(
    `V${subNumUser + iRow - 1}`
  ).value = `Ngày …… tháng …… năm 20……`;

  sheet.mergeCells(`X${subNumUser + iRow + 1}:AD${subNumUser + iRow + 1}`);
  sheet.getCell(`X${subNumUser + iRow + 1}`).font = boldFont;
  sheet.getCell(`X${subNumUser + iRow + 1}`).alignment = cenMid;
  sheet.getCell(`X${subNumUser + iRow + 1}`).value = `Phụ trách bộ phận`;

  sheet.mergeCells(`X${subNumUser + iRow + 2}:AD${subNumUser + iRow + 2}`);
  sheet.getCell(`X${subNumUser + iRow + 2}`).font = italicFont;
  sheet.getCell(`X${subNumUser + iRow + 2}`).alignment = cenMid;
  sheet.getCell(`X${subNumUser + iRow + 2}`).value = `(Ký, họ tên)`;

  sheet.mergeCells(`AH${subNumUser + iRow + 1}:AN${subNumUser + iRow + 1}`);
  sheet.getCell(`AH${subNumUser + iRow + 1}`).font = boldFont;
  sheet.getCell(`AH${subNumUser + iRow + 1}`).alignment = cenMid;
  sheet.getCell(`AH${subNumUser + iRow + 1}`).value = `Giám đốc`;

  sheet.mergeCells(`AH${subNumUser + iRow + 2}:AN${subNumUser + iRow + 2}`);
  sheet.getCell(`AH${subNumUser + iRow + 2}`).font = italicFont;
  sheet.getCell(`AH${subNumUser + iRow + 2}`).alignment = cenMid;
  sheet.getCell(`AH${subNumUser + iRow + 2}`).value = `(Ký, họ tên)`;
};

module.exports = studentAttend;
