const catchAsync = require("../utils/catchAsync");
const Models = require("../models/sequelize");

class StudentController {
  constructor(sequelize) {
    Models(sequelize);
    this.client = sequelize;
    this.models = sequelize.models;
  }

  getAllStudent = catchAsync(async (req, res, next) => {
    const doc = await this.models.Student.findAll({
      attributes: ["id", "code", "name"],
      order: [["id", "ASC"]],
    });
    res.status(200).json({
      status: "success",
      data: doc,
    });
  });
}

module.exports = StudentController;
