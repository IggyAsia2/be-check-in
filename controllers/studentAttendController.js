const catchAsync = require("../utils/catchAsync");
const AppError = require("../utils/appError");
const Models = require("../models/sequelize");
const studentAttend = require("../templates/studentAttend");
const excelJS = require("exceljs");

class StudentAttendController {
  constructor(sequelize) {
    Models(sequelize);
    this.client = sequelize;
    this.models = sequelize.models;
  }

  getStudentAttends = catchAsync(async (req, res, next) => {
    const doc = await this.models.StudentAttendance.findAll({
      attributes: { exclude: ["studentid", "StudentId"] },
      include: [
        {
          model: this.models.Student,
          attributes: ["id", "code", "name"],
        },
      ],
    });
    res.status(200).json({
      status: "success",
      data: doc,
    });
  });

  containsObject(obj, list) {
    var i;
    for (i = 0; i < list.length; i++) {
      if (list[i] === obj) {
        return true;
      }
    }

    return false;
  }

  createStudentAttend = catchAsync(async (req, res, next) => {
    const { date } = req.body;
    const date1 = new Date(date[0]);
    const date2 = new Date(date[1]);
    const difference = date2.getTime() - date1.getTime();
    const days = Math.ceil(difference / (1000 * 3600 * 24));
    const arr = [];
    const holiday = await this.models.Date.findAll({
      order: [["date", "ASC"]],
    });
    const holidayArr = holiday.map((el) => el.date.toString());
    for (let i = 0; i <= days; i++) {
      if (
        date1.getDay() != 6 &&
        date1.getDay() != 0 &&
        !this.containsObject(date1.toString(), holidayArr)
      ) {
        await this.models.StudentAttendance.create({
          ...req.body,
          date: date1,
        });
      }
      date1.setDate(date1.getDate() + 1);
    }
    res.status(201).json({
      status: "success",
    });
  });

  updateStudentAttend = catchAsync(async (req, res, next) => {
    const doc = await this.models.StudentAttendance.update(req.body, {
      where: { id: req.params.id },
    });
    res.status(200).json({
      status: "success",
      data: doc,
    });
  });

  deleteStudentAttend = catchAsync(async (req, res, next) => {
    const doc = await this.models.StudentAttendance.destroy({
      where: { id: req.params.id },
    });
    if (!doc) {
      return next(new AppError(`No date found with that ID`, 404));
    }
    res.status(200).json({
      status: "success",
      data: null,
    });
  });

  monthDiff = (date1, date2) => {
    const d1 = new Date(date1);
    const d2 = new Date(date2);
    let months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth();
    months += d2.getMonth() + 1;
    return months <= 0 ? 0 : months;
  };

  exportExcelFile = catchAsync(async (req, res, next) => {
    const option = await this.client.query(
      `select * from t_option where optiongroupid = 1`
    );
    const options = option[0];

    const workbook = new excelJS.Workbook();
    const paths = "./files";
    const { fromDay, toDay } = req.query;
    const numMonth = this.monthDiff(fromDay, toDay);
    const inputYear = new Date(fromDay).getFullYear();
    const monthArr = [];
    for (let i = 0; i < numMonth; i++) {
      monthArr.push({
        from: new Date(fromDay),
        to: new Date(toDay),
        fromDay,
        toDay,
      });
    }

    // monthArr.forEach(async (el, index) => {

    // });

    for (let el of monthArr) {
      const from_day = el.from;
      const doc1 = await this.client.query(
        `select * from fn_thongke_diemdanh_theongay('${el.fromDay}', '${el.toDay}') where orgunit = 'CEC'`
      );
      const doc2 = await this.client.query(
        `select * from fn_thongke_diemdanh_theongay('${el.fromDay}', '${el.toDay}') where orgunit = 'BMES'`
      );
      const doc3 = await this.client.query(
        `select * from fn_thongke_diemdanh_theongay('${el.fromDay}', '${el.toDay}') where orgunit = 'TRIS'`
      );
      const doc4 = await this.client.query(
        `select * from fn_thongke_diemdanh_theongay('${el.fromDay}', '${el.toDay}') where orgunit = 'DIGIHUBS'`
      );
      const bigData = [doc1[0], doc2[0], doc3[0], doc4[0]];

      const arrDays = [];
      const date1 = el.from;
      const date2 = el.to;
      const difference = date2.getTime() - date1.getTime();
      const days = Math.ceil(difference / (1000 * 3600 * 24));
      for (let i = 0; i <= days; i++) {
        arrDays.push(date1.toString());
        date1.setDate(date1.getDate() + 1);
      }

      const numNL = doc1[0]
        .slice(0, days + 1)
        .filter(
          (el) =>
            el.attendance === "T7" ||
            el.attendance === "CN" ||
            el.attendance === "NL"
        ).length;

      studentAttend({
        from_day,
        days,
        arrDays,
        bigData,
        workbook,
        options,
        numNL,
      });
    }

    try {
      await workbook.xlsx.writeFile(`${paths}/userAttendance.xlsx`).then(() => {
        const file = `${paths}/userAttendance.xlsx`;
        res.download(file);
      });
    } catch (err) {
      console.log(err);
      res.send(err);
    }
  });

  getAllStudentAtttend = catchAsync(async (req, res, next) => {
    const { fromDay, toDay } = req.params;
    if (fromDay !== "null" && toDay !== "null") {
      const date1 = new Date(fromDay);
      const date2 = new Date(toDay);
      const difference = date2.getTime() - date1.getTime();
      const days = Math.ceil(difference / (1000 * 3600 * 24));
      const doc1 = await this.client.query(
        `select * from fn_thongke_diemdanh_theongay('${fromDay}', '${toDay}') where orgunit = 'CEC'`
      );

      const doc2 = await this.client.query(
        `select * from fn_thongke_diemdanh_theongay('${fromDay}', '${toDay}') where orgunit = 'BMES'`
      );
      const doc3 = await this.client.query(
        `select * from fn_thongke_diemdanh_theongay('${fromDay}', '${toDay}') where orgunit = 'TRIS'`
      );
      const doc4 = await this.client.query(
        `select * from fn_thongke_diemdanh_theongay('${fromDay}', '${toDay}') where orgunit = 'DIGIHUBS'`
      );

      const bigData = [...doc1[0], ...doc2[0], ...doc3[0], ...doc4[0]];

      Array.prototype.chunk = function (size) {
        let result = [];

        while (this.length) {
          result.push(this.splice(0, size));
        }

        return result;
      };

      const newArr = bigData.chunk(days + 1);
      let arr = [];

      newArr.forEach((el, bigIndex) => {
        let obj = {
          studentname: el[0].studentname,
          studentid: el[0].student_id,
          orgunit: el[0].orgunit,
          id: bigIndex + 1,
        };
        el.forEach((item, index) => {
          Object.assign(obj, {
            [new Date(item.eventdate).getDate()]: {
              attendance_id: item.attendance_id,
              val: item.attendance,
              eventdate: item.eventdate,
              eventtime: item.eventtime,
              imgurl: item.imgurl,
              note: item.attendance_note,
              isChange: false,
            },
          });
        });
        arr.push(obj);
        obj = {};
      });

      res.status(200).json({
        status: "success",
        data: arr,
      });
    } else {
      res.status(200).json({
        status: "success",
        data: [],
      });
    }
  });

  getColDate = catchAsync(async (req, res, next) => {
    const { fromDay, toDay } = req.params;
    if (fromDay !== "null" && toDay !== "null") {
      const options = await this.client.query(
        `select * from t_option where optiongroupid = 1`
      );
      const col = [];
      for (
        let i = new Date(fromDay).getDate();
        i <= new Date(toDay).getDate();
        i++
      ) {
        col.push({ d: i });
      }
      res.status(200).json({
        status: "success",
        data: col,
        options: options[0],
      });
    } else {
      res.status(200).json({
        status: "success",
        data: [],
      });
    }
  });

  saveStudentAttend = catchAsync(async (req, res, next) => {
    await this.models.StudentAttendance.bulkCreate(req.body.data);
    req.body.data2.forEach(async (el) => {
      await this.models.StudentAttendance.update(
        { status: el.status },
        {
          where: { id: el.id },
        }
      );
    });
    // console.log(req.body);
    res.status(201).json({
      status: "success",
    });
  });
}

module.exports = StudentAttendController;
