const express = require("express");
const router = express.Router();

const StudentAttendController = require("../../controllers/studentAttendController");

module.exports = (config) => {
  const studentAttendController = new StudentAttendController(
    config.postgres.client
  );

  router
    .route("/")
    .get(studentAttendController.getStudentAttends)
    .post(studentAttendController.createStudentAttend);

  router
    .route("/:id")
    .patch(studentAttendController.updateStudentAttend)
    .delete(studentAttendController.deleteStudentAttend);

  router.post("/save", studentAttendController.saveStudentAttend);

  router.get("/downloadExcel", studentAttendController.exportExcelFile);
  router.get(
    "/allAttendance/:fromDay/:toDay",
    studentAttendController.getAllStudentAtttend
  );
  router.get("/dates/:fromDay/:toDay", studentAttendController.getColDate);
  return router;
};
