const express = require("express");
const router = express.Router();

const OptionController = require("../../controllers/optionController");

module.exports = (config) => {
  const optionController = new OptionController(config.postgres.client);

  router.route("/:groupId").get(optionController.getAllOption);

  return router;
};
