const express = require("express");
const router = express.Router();

const specialDayRoute = require("./specialDay");
const studentRoute = require("./student");
const studentAttendRoute = require("./studentAttend");
const optionRoute = require("./option");

module.exports = (config) => {
  // router.get('/', (req, res) => {
  //   res.send('Home Page');
  // });

  router.use("/days", specialDayRoute(config));
  router.use("/students", studentRoute(config));
  router.use("/student-attends", studentAttendRoute(config));
  router.use("/options", optionRoute(config));

  return router;
};
